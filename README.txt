This is a package of datasets

Description of Datasets :
========================

(A) Microarray, Annotation and Gene Lists :
===========================================

Microarray data for real expts:
-------------------------------
1. expr.WT    : WT Gene Expression
2. expr.WTBL  : WT+BL Gene Expression


Probe v. Symbol Files :
-----------------------
1. probe2symbol  : Mapping between ATH1 probe and A. thaliana gene identifier
2. symbol2probe  : Mapping between A. thaliana gene identifier and ATH1 probe

Transcription Factors data :
----------------------------
1. btf300  : 300 Brassinoid TF list
2. atf2492 : A comprehensive list of 2492 Arabidopsis TFs


(B) Whole-Genome Networks :
===========================

The following are the 15 (5+7+3) networks constructed by the mututal information method
called "TINGe"

Tissue Networks w. Weights :
----------------------------
1. flower      : Network and the MI values corresponding to flower dataset
2. leaf        : Network and the MI values corresponding to leaf dataset
3. root        : Network and the MI values corresponding to root dataset
4. seedling    : Network and the MI values corresponding to seedling dataset
5. whole.plant : Network and the MI values corresponding to whole plant dataset

Process Networks w. Weights :
-----------------------------
1. chemical    : Network and the MI values corresponding to chemical dataset
2. development : Network and the MI values corresponding to development dataset
3. hormone     : Network and the MI values corresponding to hormone dataset
4. light       : Network and the MI values corresponding to light dataset
5. metabolism  : Network and the MI values corresponding to metabolism dataset
6. pathogen    : Network and the MI values corresponding to pathogen dataset
7. stress      : Network and the MI values corresponding to stress dataset

Whole Genome Union Network :
----------------------------
1. full.union : Union of all 12 networks.
2. tissue.union : Union of all tissue networks.
3. process.union : Union of all process networks.


The following 15 (5+7+3) networks are the same as the above networks i.e., they have the
same vertexes and edges - only that the weight is Pearson Correlation Coefficients (PCC).

Tissue Networks w. PCC as weights:
-----------------------------------
1. pcc.flower      : Network and the PCC values corresponding to flower dataset
2. pcc.leaf        : Network and the PCC values corresponding to leaf dataset
3. pcc.root        : Network and the PCC values corresponding to root dataset
4. pcc.seedling    : Network and the PCC values corresponding to seedling dataset
5. pcc.whole.plant : Network and the PCC values corresponding to whole plant dataset

Process Networks w. PCC as weights :
------------------------------------
1. pcc.chemical    : Network and the PCC values corresponding to chemical dataset
2. pcc.development : Network and the PCC values corresponding to development dataset
3. pcc.hormone     : Network and the PCC values corresponding to hormone dataset
4. pcc.light       : Network and the PCC values corresponding to light dataset
5. pcc.metabolism  : Network and the PCC values corresponding to metabolism dataset
6. pcc.pathogen    : Network and the PCC values corresponding to pathogen dataset
7. pcc.stress      : Network and the PCC values corresponding to stress dataset

Whole Genome Union Network :
----------------------------
1. pcc.full.union : Union of all 12 networks.
2. pcc.tissue.union : Union of all tissue networks.
3. pcc.process.union : Union of all process networks.

(C) TF-Targeted Whole-Genome Networks :
=======================================
The following 15 (5+7+3) networks are generated using TINGe, but the TF-target for
the 2492 transcription factors are not eliminated.

Tissue TF-targeted Networks w. Weights :
----------------------------------------
1. tfs.flower      : Network and the MI values corresponding to flower dataset
2. tfs.leaf        : Network and the MI values corresponding to leaf dataset
3. tfs.root        : Network and the MI values corresponding to root dataset
4. tfs.seedling    : Network and the MI values corresponding to seedling dataset
5. tfs.whole.plant : Network and the MI values corresponding to whole plant dataset

Process TF-targeted Networks w. Weights :
-----------------------------------------
1. tfs.chemical    : Network and the MI values corresponding to chemical dataset
2. tfs.development : Network and the MI values corresponding to development dataset
3. tfs.hormone     : Network and the MI values corresponding to hormone dataset
4. tfs.light       : Network and the MI values corresponding to light dataset
5. tfs.metabolism  : Network and the MI values corresponding to metabolism dataset
6. tfs.pathogen    : Network and the MI values corresponding to pathogen dataset
7. tfs.stress      : Network and the MI values corresponding to stress dataset

Whole Genome Union Network :
----------------------------
1. tfs.full.union : Union of all 12 networks.
2. tfs.tissue.union : Union of all tissue networks.
3. tfs.process.union : Union of all process networks.

The following networks 15(5+7+3) are the same as the above networks i.e., they have the
same vertexes and edges - only that the weight is Pearson Correlation Coefficients (PCC).

Tissue Networks w. PCC as weights:
-----------------------------------
1. tfs.pcc.flower      : Network and the PCC values corresponding to flower dataset
2. tfs.pcc.leaf        : Network and the PCC values corresponding to leaf dataset
3. tfs.pcc.root        : Network and the PCC values corresponding to root dataset
4. tfs.pcc.seedling    : Network and the PCC values corresponding to seedling dataset
5. tfs.pcc.whole.plant : Network and the PCC values corresponding to whole plant dataset

Process Networks w. PCC as weights :
------------------------------------
1. tfs.pcc.chemical    : Network and the PCC values corresponding to chemical dataset
2. tfs.pcc.development : Network and the PCC values corresponding to development dataset
3. tfs.pcc.hormone     : Network and the PCC values corresponding to hormone dataset
4. tfs.pcc.light       : Network and the PCC values corresponding to light dataset
5. tfs.pcc.metabolism  : Network and the PCC values corresponding to metabolism dataset
6. tfs.pcc.pathogen    : Network and the PCC values corresponding to pathogen dataset
7. tfs.pcc.stress      : Network and the PCC values corresponding to stress dataset

Whole Genome Union Network :
----------------------------
1. tfs.pcc.full.union : Union of all 12 networks.
2. tfs.pcc.tissue.union : Union of all tissue networks.
3. tfs.pcc.process.union : Union of all process networks.


(D) TF-Targeted Whole-Genome Networks using GENIE3:
===================================================
The following 15 (5+7+3) networks are generated using GENIE3, run with
the 2492 transcription factors as the regulators. Only edges with
weights >= 0.004 are provided to have network density comparable to tfs.pcc
networks.

Tissue TF-targeted GENIE3 Networks w. Weights :
-----------------------------------------------
1. tfs.g3.flower      : Network and the GEINE3 weights corresponding to flower dataset
2. tfs.g3.leaf        : Network and the GEINE3 weights corresponding to leaf dataset
3. tfs.g3.root        : Network and the GEINE3 weights corresponding to root dataset
4. tfs.g3.seedling    : Network and the GEINE3 weights corresponding to seedling dataset
5. tfs.g3.whole.plant : Network and the GEINE3 weights corresponding to whole plant dataset

Process TF-targeted GENINE Networks w. Weights :
------------------------------------------------
1. tfs.g3.chemical    : Network and the GEINE3 weights corresponding to chemical dataset
2. tfs.g3.development : Network and the GEINE3 weights corresponding to development dataset
3. tfs.g3.hormone     : Network and the GEINE3 weights corresponding to hormone dataset
4. tfs.g3.light       : Network and the GEINE3 weights corresponding to light dataset
5. tfs.g3.metabolism  : Network and the GEINE3 weights corresponding to metabolism dataset
6. tfs.g3.pathogen    : Network and the GEINE3 weights corresponding to pathogen dataset
7. tfs.g3.stress      : Network and the GEINE3 weights corresponding to stress dataset

Whole Genome Union GENIE3 Networks :
------------------------------------
1. tfs.g3.full.union : Union of all 12 networks.
2. tfs.g3.tissue.union : Union of all tissue networks.
3. tfs.g3.process.union : Union of all process networks.


(E) Mapped TF-TARGET Networks
=============================

Whole Genome Union mapped to AT gene ids Networks :
---------------------------------------------------
1. tfs.tinge.union.mapped  : TINGe union network (mapped)
2. tfs.g3.union.mapped : GENIE3 union network (mapped)
3. tfs.grnboost.union.mapped : GRNBosst union network (mapped)

Using Arabidopsis Whole Genome Networks :
=========================================

1. Install the following R libraries : devtools

2. Install the wgn.athaliana library using devtools

> devtools::install_bitbucket("srirampc/wgn.athaliana")

3. Load all the Arabidopsis Annotation and all the union network files

> wgn.athaliana::load.wgn()

  To load a specific dataset, say chemical, the following command will work.

> data(wgn.athaliana::chemical)
